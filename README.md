# Magewell Ultra Stream

This is a Q-SYS Plugin for the Magewell Ultra Stream.

> Bug reports and feature requests should be sent to Jason Foord (jf@tag.com.au).

## How do I get set up?

See [Q-SYS Online Help File - Plugins](https://q-syshelp.qsc.com/#Schematic_Library/plugins.htm)


## Properties

*This Plugin has no configurable properties.*

## Controls

### Dashboard Page
![Dashboard Page](./Screenshots/Ultra%20Stream%20Dashboard.JPG)

#### Device Status

Displays the device's status.

#### Serial Number

Displays the device's serial number.

#### Unit Name

Displays the device's unit name.

#### Unit Type

Displays the device's unit type.

#### FW Version

Displays the device's firmware version.

#### CPU Graph

An SVG graph showing CPU usage.

#### CPU Usage

The current CPU usage percentage.

#### CPU

Further CPU information.

#### USB Status

The status of the connected USB storage device, if any.

#### USB

**Graph**

An SVG graph showing the read and write speeds.

**Read Speed**

The current read speed of the storage device.

**Write Speed**

The current write speed of the storage device.

**Space Used**

The storage space that has been used.

**Space Available**

The storage space available.

**Space Total**

The total storage capacity of the device.

#### SD

**Graph**

An SVG graph showing the read and write speeds.

**Read Speed**

The current read speed of the storage device.

**Write Speed**

The current write speed of the storage device.

**Space Used**

The storage space that has been used.

**Space Available**

The storage space available.

**Space Total**

The total storage capacity of the device.

#### Start Recording

Start recording.

#### Stop Recording

Stop recording.

#### Recording State

The current recording state.

#### Stream Status

The streaming status.

#### Start Streaming

Start streaming.

#### Stop Streaming

Stop streaming.

#### Streaming State

The current streaming state.

### Setup Page
![Setup Page](./Screenshots/Ultra%20Stream%20Setup.JPG)

#### Error Status

Displays the current number of errors, if any.

#### Session Status

Displays the name of the user account that is currently logged in.

#### ID

The user ID to log in with.

#### Password

The user password to log in with.

#### IP Address

The IP Address of the device.

#### Connect

Toggles the connection to the device.

#### Error Log

A current log of errors for the most recent result of each HTTP request.

> The last result for each request type will overwrite any previous errors. If the last result of a request was ***OK*** then the error will disappear from this list.